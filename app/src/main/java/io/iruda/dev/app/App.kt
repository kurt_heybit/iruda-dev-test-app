package io.iruda.dev.app

import android.app.Application
import com.preference.PowerPreference

class App : Application() {
    override fun onCreate() {
        super.onCreate()

        PowerPreference.init(this)
        val default = mapOf(
//            "home_url" to "http://d12qd56mp1j02q.cloudfront.net/test/home",
            "home_url" to "http://d12qd56mp1j02q.cloudfront.net/test/home",
            "headers" to "{\"heybit\"=\"uprise\"}",
            "cookie_domain" to "https://www.iruda.io",
            "cookies" to "{ \"heybit\"=\"uprise\" }"
        )
        PowerPreference.getDefaultFile().setDefaults(default)
    }
}