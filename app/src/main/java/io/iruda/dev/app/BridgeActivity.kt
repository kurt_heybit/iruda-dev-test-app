package io.iruda.dev.app

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.webkit.CookieManager
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.blogspot.atifsoftwares.animatoolib.Animatoo
import com.github.lzyzsd.jsbridge.CallBackFunction
import com.github.lzyzsd.jsbridge.DefaultHandler
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import kotlinx.android.synthetic.main.activity_bridge.*
import kotlinx.android.synthetic.main.content_bridge.*
import java.lang.IllegalArgumentException


abstract class BridgeActivity : AppCompatActivity() {

    companion object {
        const val KEY_RESULT = "result"
        const val KEY_URL = "url"
        const val KEY_TITLE = "title"
        const val KEY_COLOR = "color"

        fun newIntent(
            context: Context,
            type: WebCallRequest.Type,
            url: String,
            title: String,
            color: String
        ): Intent {

            val targetActivity = when (type) {
                WebCallRequest.Type.PAGE_REPLACE -> RootWebViewActivity::class.java
                WebCallRequest.Type.PAGE_DIALOG -> DialogWebViewActivity::class.java
                else -> PushWebViewActivity::class.java
            }

            return Intent(context, targetActivity).apply {
                putExtra(KEY_URL, url)
                putExtra(KEY_TITLE, title)
                putExtra(KEY_COLOR, color)

                if (type == WebCallRequest.Type.PAGE_REPLACE) {
                    flags =
                        Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                }
            }
        }
    }

    private val TAG = "BridgeActivityTest"

    private val gson: Gson by lazy { Gson() }
    val url: String by lazy {
        intent.getStringExtra(KEY_URL) ?: "http://d12qd56mp1j02q.cloudfront.net/test/bridge"
    }
    private val title: String? by lazy { intent.getStringExtra(KEY_TITLE) }
    private val color: Int? by lazy {
        val colorString = intent.getStringExtra(KEY_COLOR)
        colorString?.let {
            try {
                Color.parseColor(colorString)
            } catch (e: IllegalArgumentException) {
                null
            }
        }
    }

    val domain: String = "http://d12qd56mp1j02q.cloudfront.net"

    private var callbackFunction: CallBackFunction? = null

    private val handler = WebCallHandler(this)

    private val cookieManager: CookieManager = CookieManager.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bridge)
        setSupportActionBar(toolbar)

        supportActionBar?.let { actionBar ->
            title?.let { actionBar.title = it }
            color?.let { actionBar.setBackgroundDrawable(ColorDrawable(it)) }
            setupActionBar(actionBar)
        }

        fab.setOnClickListener { view ->
            webView.callHandler(
                "functionInJs", "data from Java"
            ) { data ->
                // TODO Auto-generated method stub
                Log.i(TAG, "reponse data from js $data")
            }
        }

        loadUrl(url)
        receiveWebCall()

        init()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    abstract fun setupActionBar(actionBar: ActionBar)

    private fun init() {
        webView.callHandler(
            "callJsHandler", null
        ) { }

        webView.send("hello")
    }

    private fun loadUrl(url: String) {
        webView.setDefaultHandler(DefaultHandler())
        webView.loadUrl(url)
    }


    private fun receiveWebCall() {
        webView.registerHandler(
            "callNativeHandler"
        ) { data, function ->
            callbackFunction = function
            Log.i(TAG, data.toString())

            val webCallRequest = gson.fromJson(data, WebCallRequest::class.java)
            handler.handleWebCall(webCallRequest, function)
            Log.i(TAG, webCallRequest.toString())
            Log.i(TAG, "call")
//            function.onCallBack("submitFromWeb exe, response data from Java")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && data != null) {
            val s = "[onActivityResult] resultCode: $resultCode, data: $data"
            Log.d(TAG, s)
            val result = data.getStringExtra(KEY_RESULT)
            callbackFunction?.onCallBack(result)
        }
    }

    fun updateCookies(data: Map<String, Any>) {
        cookieManager.removeAllCookies {
            data.forEach { (key, value) -> cookieManager.setCookie(domain, "$key=$value") }
            Log.d("cookies", cookieManager.getCookie(domain))
        }
    }

    object RequestCode {
        const val PAGE_PUSH = 0
        const val PAGE_DIALOG = 1
    }
}


class WebCallHandler(private var mContext: BridgeActivity) {
    private val gson: Gson by lazy { Gson() }

    fun handleWebCall(
        webCallRequest: WebCallRequest?,
        function: CallBackFunction
    ) {
        webCallRequest?.let {
            when (webCallRequest.type) {
                WebCallRequest.Type.PAGE_PUSH -> handlePagePush(webCallRequest.data, function)
                WebCallRequest.Type.PAGE_POP -> handlePagePop(webCallRequest.data, function)
                WebCallRequest.Type.PAGE_REPLACE -> handlePageReplace(webCallRequest.data, function)
                WebCallRequest.Type.PAGE_DIALOG -> handlePageDialog(webCallRequest.data, function)

                WebCallRequest.Type.TOAST -> handleToast(webCallRequest.data, function)
                WebCallRequest.Type.ALERT -> handleAlert(webCallRequest.data, function)
                WebCallRequest.Type.SET_COOKIE -> handleCookie(webCallRequest.data, function)

                else -> mContext.toast("type: ${webCallRequest.type.toString()}, data: ${webCallRequest.data.toString()}")
            }
        }

    }

    fun handlePagePush(
        data: Map<String, Any>,
        function: CallBackFunction
    ) {
        mContext.toast(data.toString())
        mContext.startActivityForResult(
            BridgeActivity.newIntent(
                mContext,
                WebCallRequest.Type.PAGE_PUSH,
                mContext.domain + data["path"],
                data["title"] as String,
                data["color"] as String
            ),
            BridgeActivity.RequestCode.PAGE_PUSH
        )
        Animatoo.animateSlideLeft(mContext)

    }

    fun handlePageDialog(
        data: Map<String, Any>,
        function: CallBackFunction
    ) {
        mContext.toast(data.toString())
        mContext.startActivityForResult(
            BridgeActivity.newIntent(
                mContext,
                WebCallRequest.Type.PAGE_DIALOG,
                mContext.domain + data["path"],
                data["title"] as String,
                data["color"] as String
            ),
            BridgeActivity.RequestCode.PAGE_DIALOG
        )
        Animatoo.animateSlideUp(mContext)
    }

    fun handlePageReplace(
        data: Map<String, Any>,
        function: CallBackFunction
    ) {
        mContext.toast(data.toString())
        mContext.startActivity(
            BridgeActivity.newIntent(
                mContext,
                WebCallRequest.Type.PAGE_REPLACE,
                mContext.domain + data["path"],
                data["title"] as String,
                data["color"] as String
            )
        )
        Animatoo.animateInAndOut(mContext)
    }

    fun handlePagePop(
        data: Map<String, Any>,
        function: CallBackFunction
    ) {
        mContext.setResult(
            Activity.RESULT_OK,
            Intent().apply { putExtra(BridgeActivity.KEY_RESULT, gson.toJson(data)) })

        mContext.finish()
        mContext.toast(data.toString())
    }

    fun handleToast(
        data: Map<String, Any>,
        function: CallBackFunction
    ) {
        mContext.toast(data.toString())
    }

    fun handleCookie(
        data: Map<String, Any>,
        function: CallBackFunction
    ) {
        mContext.toast(data.toString())
        mContext.updateCookies(data)
    }

    fun handleAlert(
        data: Map<String, Any>,
        function: CallBackFunction
    ) {
        AlertDialog.Builder(mContext).apply {
            val message = data["message"] as String
            val buttons = data["buttons"] as Map<*, *>
            val positive = buttons["positive"] as? Map<*, *>
            val negative = buttons["negative"] as? Map<*, *>

            setMessage(message)
            // positive 는 항상 줄꺼에요..
            positive?.let {
                setPositiveButton(it["label"] as String) { _, _ ->
                    // "buttons.positive.value"
                    Log.d("test", it.toString())
                    function.onCallBack(gson.toJson(it["value"]))
                }
            }

            // negative 있을 때만..
            negative?.let {
                setNegativeButton(it["label"] as String) { _, _ ->
                    function.onCallBack(gson.toJson(it["value"]))
                }
            }
        }.show()
    }
}


data class WebCallRequest(val type: Type?, val data: Map<String, Any>) {
    enum class Type {
        @SerializedName("pagePush")
        PAGE_PUSH,
        @SerializedName("pageDialog")
        PAGE_DIALOG,
        @SerializedName("pageReplace")
        PAGE_REPLACE,
        @SerializedName("pagePop")
        PAGE_POP,
        @SerializedName("toast")
        TOAST,
        @SerializedName("setCookie")
        SET_COOKIE,
        @SerializedName("alert")
        ALERT
    }
}

fun Activity.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

class RootWebViewActivity : BridgeActivity() {
    override fun setupActionBar(actionBar: ActionBar) {

    }

    override fun finish() {
        super.finish()
        Animatoo.animateInAndOut(this)
    }
}

class DialogWebViewActivity : BridgeActivity() {
    override fun setupActionBar(actionBar: ActionBar) {
        val drawable = resources.getDrawable(R.drawable.ic_clear_24px)
            .apply { setTint(Color.WHITE) }
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setHomeAsUpIndicator(drawable)
    }

    override fun finish() {
        super.finish()
        Animatoo.animateSlideDown(this)
    }
}

class PushWebViewActivity : BridgeActivity() {
    override fun setupActionBar(actionBar: ActionBar) {
        actionBar.setDisplayHomeAsUpEnabled(true)
    }

    override fun finish() {
        super.finish()
        Animatoo.animateSlideRight(this)
    }
}