package io.iruda.dev.app

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.*
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.WhichButton
import com.afollestad.materialdialogs.actions.setActionButtonEnabled
import com.afollestad.materialdialogs.input.getInputField
import com.afollestad.materialdialogs.input.input
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import com.google.gson.reflect.TypeToken
import com.preference.PowerPreference
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.net.URL


class MainActivity : AppCompatActivity(), ActionBarHandler {
    companion object Prefs {
        private const val EXTRA_URL = "url"
        private const val EXTRA_POPPED = "popped"
        private const val HOME_URL = "home_url"
        private const val COOKIES = "cookies"
        private const val HEADERS = "headers"
        private const val TOKEN = "TOKEN"

        val gson: Gson = Gson()
        private val cookieManager: CookieManager = CookieManager.getInstance()

        var homeUrl: String
            get() = PowerPreference.getDefaultFile().getString(HOME_URL)
            set(value) {
                PowerPreference.getDefaultFile().putString(HOME_URL, value)
            }

        var headers: String
            get() = PowerPreference.getDefaultFile().getString(HEADERS)
            set(value) {
                PowerPreference.getDefaultFile().putString(HEADERS, value)
            }

        val headerMap: Map<String, String>
            get() = gson.fromJson(headers, object : TypeToken<Map<String, Any>>() {}.type)

        var cookies: String
            get() = PowerPreference.getDefaultFile().getString(COOKIES)
            set(value) {
                PowerPreference.getDefaultFile().putString(COOKIES, value)
                updateCookies()
            }

        private val cookieMap: Map<String, String>
            get() = gson.fromJson(cookies, object : TypeToken<Map<String, Any>>() {}.type)

        var token: String
            get() = PowerPreference.getDefaultFile().getString(TOKEN)
            set(value) {
                PowerPreference.getDefaultFile().putString(TOKEN, value)
                updateCookies()
            }

        private val domain get() = URL(homeUrl).let { "${it.protocol}://${it.host}" }

        private val baseUrl: String
            get() = URL(homeUrl).let {
                "${it.protocol}://${it.host}${if (it.port == -1) "" else ":${it.port}"}"
            }

        private fun updateCookies() {
            cookieManager.removeAllCookies {
                val domain = domain
                cookieMap.forEach { (key, value) -> cookieManager.setCookie(domain, "$key=$value") }
                if (token.isNotBlank())
                    cookieManager.setCookie(domain, "Authorization=$token")

                Log.d("cookies", cookieManager.getCookie(domain))
            }
        }

        fun newIntent(context: Context, path: String?): Intent {
            val intent = Intent(context, MainActivity::class.java)
            intent.putExtra(EXTRA_URL, "$baseUrl$path")
            intent.putExtra(EXTRA_POPPED, true)
            return intent
        }
    }

    private val url: String by lazy { intent.getStringExtra(EXTRA_URL) ?: homeUrl }
    private val popped: Boolean by lazy { intent.getBooleanExtra(EXTRA_POPPED, false) }

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        updateHomeButton()

        urlEditText.inputType = EditorInfo.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS
        urlEditText.imeOptions = EditorInfo.IME_ACTION_GO
        urlEditText.setOnEditorActionListener { textView, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_GO) {
                loadUrl(textView.text.toString())
                true
            } else false
        }

        refreshButton.setOnClickListener { reload() }
        homeButton.setOnClickListener { loadUrl(homeUrl) }

        webView.webViewClient = IrudaWebViewClient(this)
        webView.settings.javaScriptEnabled = true
        WebView.setWebContentsDebuggingEnabled(true)
        cookieManager.setAcceptCookie(true)
        cookieManager.setAcceptThirdPartyCookies(webView, true)
        webView.addJavascriptInterface(WebAppInterface(this), "Android")

        updateCookies()
        loadUrl(url)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_edit_home -> {
                showEditHomeDialog()
                true
            }

            R.id.action_edit_header -> {
                showEditHeaderDialog()
                true
            }

            R.id.action_edit_cookies -> {
                showEditCookiesDialog()
                true
            }

            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onBackPressed() {
        if (webView.canGoBack())
            webView.goBack()
        else
            super.onBackPressed()
    }

    override fun updateHomeButton() {
        supportActionBar?.run {
            setHomeButtonEnabled(popped)
            setDisplayHomeAsUpEnabled(popped)
        }
    }

    private fun loadUrl(url: String) {
        urlEditText.setText(url, TextView.BufferType.EDITABLE)
        hideKeyboard(urlEditText)
        webView.loadUrl(url, headerMap)
    }

    private fun reload(): Boolean {
        loadUrl(urlEditText.text.toString())
        return true
    }

    private fun hideKeyboard(v: TextView) {
        (v.context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
            v.windowToken,
            0
        )
    }

    private fun showEditHeaderDialog() {
        MaterialDialog(this).show {
            title(text = "Edit Headers")
            message(text = "ex) { \"key\"=\"value\"}")
            input(
                prefill = headers,
                waitForPositiveButton = false
            ) { dialog, text ->
                val jsonString = text.toString()
                val inputField = dialog.getInputField()
                val isValid = isJSONValid(jsonString)

                inputField.error = if (isValid) null else "Invalid json format"
                dialog.setActionButtonEnabled(WhichButton.POSITIVE, isValid)
                if (isValid)
                    headers = jsonString
            }
            positiveButton(text = "DONE") { reload() }
            negativeButton(text = "CLOSE") { dismiss() }
            cancelOnTouchOutside(false)
        }
    }

    private fun showEditHomeDialog() {
        MaterialDialog(this)
            .show {
                title(text = "Edit Home Url")
                input(
                    prefill = homeUrl,
                    inputType = InputType.TYPE_TEXT_VARIATION_WEB_EMAIL_ADDRESS
                ) { _, text ->
                    homeUrl = text.toString()
                }
                positiveButton(text = "DONE")
                negativeButton(text = "CLOSE") { dismiss() }
                cancelOnTouchOutside(false)
            }
    }

    private fun isJSONValid(jsonInString: String?): Boolean {
        return try {
            gson.fromJson(jsonInString, Any::class.java)
            true
        } catch (ex: JsonSyntaxException) {
            false
        }
    }

    private fun showEditCookiesDialog() {
        MaterialDialog(this).show {
            title(text = "Edit Cookies")
            message(text = "ex) { \"key\"=\"value\"}")
            input(
                prefill = cookies,
                waitForPositiveButton = false
            ) { dialog, text ->
                val jsonString = text.toString()
                val inputField = dialog.getInputField()
                val isValid = isJSONValid(jsonString)

                inputField.error = if (isValid) null else "Invalid json format"
                dialog.setActionButtonEnabled(WhichButton.POSITIVE, isValid)
                if (isValid)
                    cookies = jsonString
            }
            positiveButton(text = "DONE") { reload() }
            negativeButton(text = "CLOSE") { dismiss() }
            cancelOnTouchOutside(false)
        }
//
//        val dialog = MaterialDialog(this)
//            .customView(R.layout.view_edit_cookie)
//            .positiveButton(text = "APPLY") {
//                val customView = it.getCustomView()
//                cookieDomain = customView.domainInput.text.toString()
//                cookies = customView.cookieInput.text.toString()
//
//                updateCookies()
//            }
//            .negativeButton(text = "CLOSE")
//            .cancelOnTouchOutside(false)
//
//        val customView = dialog.getCustomView()
//        customView.domainInput.setText(cookieDomain)
//        customView.cookieInput.setText(cookies)
//
//        dialog.show()
    }
}

interface ActionBarHandler {
    fun updateHomeButton()
}

class IrudaWebViewClient(private val actionBarHandler: ActionBarHandler) : WebViewClient() {

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
        request?.let { view?.loadUrl(it.url.toString(), MainActivity.headerMap) }
        return true
    }

    override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
        view?.loadUrl(url, MainActivity.headerMap)
        return true
    }

    override fun onPageFinished(view: WebView?, url: String?) {
        super.onPageFinished(view, url)
        view?.let { actionBarHandler.updateHomeButton() }
    }
}

class WebAppInterface(private var mContext: Context) {
    // Show a toast from the web page
    @JavascriptInterface
    fun showToast(toast: String?) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show()
    }

    @JavascriptInterface
    fun pageMove(path: String?) {
        Toast.makeText(mContext, path, Toast.LENGTH_SHORT).show()
        mContext.startActivity(MainActivity.newIntent(mContext, path))
    }

    @JavascriptInterface
    fun setToken(token: String?) {
        Toast.makeText(mContext, "Set Token: $token", Toast.LENGTH_SHORT).show()
        MainActivity.token = token.orEmpty()
    }
}
